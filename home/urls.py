from django.urls import path
from .views import home
from django.contrib.auth.views import LogoutView

urlpatterns = [
    path('', home, name="home"),
    path('logout/', LogoutView.as_view(template_name="index.html"), name="logout"),
]