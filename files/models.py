from django.db import models

class File(models.Model):
    archive = models.FileField(upload_to='files', null=True, blank=True)

    def __str__(self):
        return self.archive.name

