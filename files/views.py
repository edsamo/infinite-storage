from django.http import HttpResponse
from django.shortcuts import render, redirect
from files.models import File
from files.forms import FileForm
from django.contrib.auth.decorators import login_required

@login_required
def myfiles(request):
    files = File.objects.all()
    form = FileForm(request.POST, request.FILES, None)
    if form.is_valid():
        form.save()
        form = FileForm()

    if (request.GET.get('DeleteButton')):
        File.objects.filter(id=request.GET.get('DeleteButton')).delete()
        redirect('myFiles.html')

    return render(request, 'myFiles.html', {'files': files, 'form': form})

