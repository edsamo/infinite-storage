from django.forms import ModelForm
from .models import File
from django.forms import TextInput
from django import forms

class FileForm(ModelForm):
    archive = forms.FileField(required=True)
    class Meta:
        model = File
        fields = ['archive']
        widgets = {'archive': forms.HiddenInput()}
